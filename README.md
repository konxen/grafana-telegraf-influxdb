The aim of this project is to create a system in Docker with Grafana, Telegraf and InfluxDB.
To do this we will use 'docker-compose' from the YAML file.

To run copy the files and folders to the destination and run from the directory:

`docker-compose up -d`

The '.env' file has the keys to the InfluxDB database. If they are changed, it will be necessary to change these keys in the Telegraf configuration as well.

The configuration is persistent so it will be necessary to create the corresponding folder set as follows:

`mkdir -p /srv/docker/influxdb/data`

`mkdir -p /srv/docker/grafana/data`

`chown 472:472 /srv/docker/grafana/data`

Grafana is accessible via web from port '3000'.

http://IP:3000

After starting the containers, InfluxDB may not start. Execute:

`docker exec -it [container_name] influx`

The environment has been tested on a Raspberry pi 3B, with Alpine Linux base system.